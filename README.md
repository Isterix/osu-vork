**(EN) HOW THE HECK DO I USE OSU VORK**

* Download all the code
* Put it all in a folder
* Adapt config.json to your needs:
  * Set "is_discord" to "true" if you will use the bot on Discord
    * In which case you need to associate a Discord Bot Token to "discord_token"
  * Set "is_telegram" to "true" if you will use the bot on Telegram
    * In which case you need to create a bot thanks to BotFather, then associate the token to "telegram_token"
  * Set "is_mastodon" to "true" if you will use the bot on Mastodon
    * In which case you need to get an access token for a Mastodon application, then associate it to "mastodon_token"
    * In which case the mastodon instance associated with "mastodon_instance" must be valid
  * You MUST get an API key from osu! (v1) and associate it to "osu_key"
  * You MUST put in any osu! ID or username, you get the ID from the URL of one's profile (for example, mine is 7276846)
  * You may delete "_comment"
* Choose whether you're going to host the bot on your computer or elsewhere
  * In the case of hosting your version of osu Vork on your computer:
    * Install node.js
    * In the W10 command prompt, do: cd [BOT_FOLDER_ADRESS] (without the [] and replacing BOT_FOLDER_ACCESS)
    * Do either:
      * npm run build
      * node index.js (recommended)
    * It will keep running normally as long as the command prompt is open and your computer is connected to the Internet
    * Either close the command prompt or do CTRL+C to kill the bot
  * In the case of hosting your version of osu Vork elsewhere:
    * Good luck! I can't figure that shit out yet
  * In the case you can't host osu Vork 24h/24: https://gitlab.com/Isterix/osu-vork/tree/manual

**EXPLAIN THE BOT PLS**

* The bot runs mainly on index.js
* It takes the important info out of config.json
* It depends of dependencies as told by package.json
* These dependencies are depending on other dependencies as told by package-lock.json
* All of the dependencies are stocked in the node_dependencies folder

This bot basically gets data daily from a osu! profile, makes a message daily out of it
and optionally posts it on a Discord channel, a Telegram channel or/and a Mastodon toot.

Note that it will send a message every 24h, 24h after it started.


**(FR) COMMENT PUIS-JE UTILISER OSU VORK**

* Téléchargez le code
* Mettez-le intégralement dans un dossier
* Adaptez config.json à vos besoins:
  * "is_discord" devra être sur "true" si vous voulez utiliser le robot sur Discord
    * Auquel cas associez un token valide à "discord_token"
  * "is_telegram" devra être sur "true" si vous voulez utiliser le robot sur Telegram
    * Auquel cas associez un token valide à "telegram_token"
  * "is_mastodon" devra être sur "true" si vous voulez utiliser le robot sur Mastodon
    * Auquel cas associez un token valide à "mastodon_token"
    * Auquel cas associez une instance mastodon valide à "mastodon_instance"
  * Il est obligatoire d'associer une clé de l'API d'osu! (v1) à "osu_key"
  * Il est obligatoire d'associer un nom ou ID d'utilisateur osu! (obtenable depuis l'URL d'un profil) à "osu_user"
  * Vous pouvez effacer "_comment"
* Choisissez si vous voulez hoster le robot sur votre ordinateur ou ailleurs
  * Dans le cas où vous choisissez d'hoster le robot sur votre ordinateur:
    * Installez node.js
    * Dans l'invite de commandes, faites: cd [DOSSIER_DU_ROBOT] (en remplaçant les [] ainsi que DOSSIER_DU_ROBOT)
    * Faites l'une des deux commandes:
      * npm run build
      * node index.js (recommandé)
    * Le robot fonctionnera normalement tant que l'invite de commandes est ouvert et que l'ordinateur est connecté à Internet
    * Pour tuer le robot, il suffit de faire CTRL+C dans l'invite ou bien de fermer l'invite de commandes
  * Si vous choisissez d'hoster le robot ailleurs:
    * Bonne chance, moi-même je n'y arrive pas pour le moment
  * Dans le cas où vous ne pouvez pas hoster le robot 24h/24: https://gitlab.com/Isterix/osu-vork/tree/manual

**EXLIQUE CE ROBOT STPPP**

* Le coeur du robot est index.js
* Ce dernier prend les infos importantes données dans config.json
* Il dépend des dépendences dictées par package.json
* Ces dépendences dépendent des dépendences dictées par package-lock.json
* Toutes les dépendences sont stockées dans le dossier node_dependencies

En gros, ce robot récupère quotidiennement des données d'un profil osu! puis crée quotidiennement un message grâce à elles.
Optionnellement, il publie ce message sur un salon Discord, une conversation Telegram ou/et un post Mastodon.

Notez qu'il prendra au robot 24h pour poster un message tous les 24h.